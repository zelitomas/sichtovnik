# Železárenský šichtovník

Ahoj! Tady jsou zdrojáky k aplikaci [železáreského šichtovníku][googleplay]. Aplikace je v Reactu, a k vybuildění verze pro Google Play používám Cordovu.

## Vývojové prostředí
### Instalace
Ke spuštění je potřeba zejména Node.js a NPM.

To třeba na Ubuntu nainstalujete 
```bash
sudo apt install npm
```

Na Fedoře to pak bude

```bash
sudo dnf install npm
```

Jakmile máme NPM, stačí spustit 

```
npm install
```

### Spuštění

Pak už ti nic nebrání použít

```
npm start
```

a otevřít [http://localhost:3000][localserver]

### Build balíčků

Build balíčků zatím není pořádne zdokumentovaný, ale protože ho zvládá udělat i CI, můžeš se inspirovat souborem [.gitlab-ci.yml](./.gitlab-ci.yml)



[googleplay]: https://play.google.com/store/apps/details?id=cz.tomaszelina.sichtovnik
[localserver]: http://localhost:3000