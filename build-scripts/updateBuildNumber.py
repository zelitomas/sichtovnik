#!/usr/bin/env python

#####
#
# Helper script originally created by Simon Prickett (https://github.com/simonprickett/cordovasetversion).
# Used to set correct build number in config.xml
#
# Author: Simon Prickett
#
#####

import codecs
import os
import sys
import getopt
from xml.dom import minidom


def isValidCordovaBuildString(buildString):
	try:
		int(buildString)
		return True
	except ValueError:
		return False

#####
#
# Open project's config.xml file and set the version number
# and build number according to the parameters.
#
#####
def setVersionAndBuildNumbers(buildNumber, configFile):
	# Determine which platforms this project has, supports
	# iOS and Android at the moment

	print 'Opening project\'s config.xml file'
	xmlTree = minidom.parse(configFile)
	widgetElem = xmlTree.getElementsByTagName('widget')

	if (len(widgetElem) == 1):
		widgetElem = widgetElem[0]
	else:
		# Failed invalid XML
		print '*****ERROR: Failed to find a single <widget> element in config.xml'
		sys.exit(1)

	print 'Setting iOS build number to ' + buildNumber
	widgetElem.setAttribute('ios-CFBundleVersion', buildNumber)

	print 'Setting Android build number to ' + buildNumber
	widgetElem.setAttribute('android-versionCode', buildNumber)

	# Persist the XML back to config.xml as utf-8
	with codecs.open(configFile, 'wb', 'utf-8') as out:
		xmlTree.writexml(out, encoding='utf-8')

#####
# Entry point, run the script...
#####
if (len(sys.argv) == 3):
	if (os.path.isfile(sys.argv[2])):
		if (isValidCordovaBuildString(sys.argv[1])):
			setVersionAndBuildNumbers(sys.argv[1], sys.argv[2])
			print sys.argv[2] + ' updated successfully'
		else:
			print '*****ERROR: buildNumber must be an integer number'
			sys.exit(1)
	else:
		print '*****ERROR: ' + sys.argv[2] + ' file does not exist'
else:
	print '*****ERROR: Expecting 2 parameters: buildNumber config.xml'
	sys.exit(1)